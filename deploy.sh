#!/bin/sh

# a sample deploy script

until java -server -Djava.library.path=./lib -Xss8m -jar target/dropwizard-starter.jar server configuration.yml ; do
    echo "Server crashed with exit code $?.  Respawning.." >&2
    sleep 1
done
