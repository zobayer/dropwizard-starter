package com.tigerit.dropwizardstarter;

import com.tigerit.dropwizardstarter.healthcheck.ConfigurationHealthCheck;
import com.tigerit.dropwizardstarter.resources.TestResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * @author  zobayer
 * @version 1.0
 * @since   12/24/14
 */
public class DropwizardApplication  extends Application<DropwizardConfiguration> {
    public static void main(String[] args) throws Exception {
        new DropwizardApplication().run(args);
    }

    @Override
    public String getName() {
        return "dropwizard-starter";
    }

    @Override
    public void initialize(Bootstrap<DropwizardConfiguration> bootstrap) {

    }

    @Override
    public void run(DropwizardConfiguration configuration, Environment environment) throws Exception {
        final TestResource resource = new TestResource (
                configuration.getTemplate(),
                configuration.getDefaultName()
        );
        final ConfigurationHealthCheck healthCheck = new ConfigurationHealthCheck(
                configuration.getTemplate(),
                configuration.getDefaultName()
        );
        environment.healthChecks().register("configuration", healthCheck);
        environment.jersey().register(resource);
    }

}
