package com.tigerit.dropwizardstarter.beans;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author  zobayer
 * @version 1.0
 * @since   12/24/14
 */
public class Response {
    private long id;
    private String content;

    public Response() {}

    public Response(long id, String content) {
        this.id = id;
        this.content = content;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public String getContent() {
        return content;
    }

    @JsonProperty
    public void setContent(String content) {
        this.content = content;
    }
}
