package com.tigerit.dropwizardstarter.healthcheck;

import com.codahale.metrics.health.HealthCheck;

/**
 * @author  zobayer
 * @version 1.0
 * @since   12/24/14
 */
public class ConfigurationHealthCheck extends HealthCheck {
    private final String template;
    private final String defaultName;

    public ConfigurationHealthCheck(String template, String defaultName) {
        this.defaultName = defaultName;
        this.template = template;
    }

    @Override
    protected Result check() throws Exception {
        final String response = String.format(template, defaultName);
        if (!response.contains(defaultName)) {
            return Result.unhealthy("Proper message telling some configuration data is inappropriate");
        }
        return Result.healthy();
    }
}
