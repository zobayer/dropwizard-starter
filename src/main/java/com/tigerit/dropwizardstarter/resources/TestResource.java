package com.tigerit.dropwizardstarter.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import com.tigerit.dropwizardstarter.beans.Response;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author  zobayer
 * @version 1.0
 * @since   12/24/14
 */
@Path("/droptest")
@Produces(MediaType.APPLICATION_JSON)
public class TestResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    public TestResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

    @GET
    @Timed
    public Response sayHello(@QueryParam("name") Optional<String> name) {
        final String value = String.format(template, name.or(defaultName));
        return new Response(counter.incrementAndGet(), value);
    }
}
